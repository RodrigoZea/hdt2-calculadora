/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * HT2 CALCULADORA
 * CalculadoraTest: Clase de JUnit para efectuar la prueba del metodo calcular, para observar si realiza bien la operacion o no
 * @author Rodrigo Zea y Francisco Molina
 */
public class CalculadoraTest {
    
    public CalculadoraTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }

    /**
     * Test of calcular method, of class Calculadora.
     */
    @Test
    public void testCalcular() {
        System.out.println("calcular");
        String exp = "1 1 +";
        Calculadora instance = new Calculadora();
        String expResult = "2.0";
        String result = instance.calcular(exp);
        assertEquals(expResult, result);
   
    }
    
}
