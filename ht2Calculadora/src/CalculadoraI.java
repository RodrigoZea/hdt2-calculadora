
/**
 * HT2 CALCULADORA
 * CalculadoraI: interfaz a ser implementada en la clase calculadora, para que esta logre efectuar calculos
 * @author Rodrigo Zea y Francisco Molina
 */
public interface CalculadoraI {

    /**
     * metodo encargado de realizar una operacion aritmetica indicada en un archivo de texto
     * @param exp: la expresion sin editar. la obtiene de un archivo txt
     * @return total: un string que almacena el total de la operacion realizada
     */
    String calcular (String exp);
}
