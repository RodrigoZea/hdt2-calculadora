
import java.util.Vector;

/**
 * HT2 CALCULADORA
 * StackVector: Un stack de vectores que permite almacenar y devolver los valores almacenados en el
 * @author Rodrigo Zea y Francisco Molina
 */
public class StackVector<E> implements Stack<E> {
    protected Vector<E> data;
    
    /**
     * constructor generico de la clase StackVector, devuelve un vector vacio
     */
    public StackVector()
    { 
        data = new Vector<E>();
    }
    
    /**
     * crea un stack vacio pero de un tamaño especifico
     * @param size: el tamano deseado del stack
     */
    public StackVector(int size){ 
        data = new Vector<E>(size);
    }

    @Override
    public void push(E item) {
        data.add(item);
    }

    @Override
    public E pop() {
        return data.remove(size() - 1);
    }

    @Override
    public E peek() {
        return data.get(size() - 1);
    }

    @Override
    public boolean empty() {
        return size() == 0;
    }

    @Override
    public int size() {
        return data.size();
    }
}
