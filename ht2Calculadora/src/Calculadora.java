
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * HT2 CALCULADORA
 * Calculadora: la clase encargada de realizar el calculo de una operacion aritmetica en un archivo de texto
 * @author Rodrigo Zea y Francisco Molina
 */
public class Calculadora implements CalculadoraI {
Stack<Double> stk = new StackVector<Double>();
  //Lector de texto
  BufferedReader reader;
    
  public Calculadora(){
        
    }
    //se agrego void porque aun no tengo muy claro que retornara.... tomarlo como placeholder.
  // solucionado.

    
    @Override
    public String calcular(String exp) {
        //Variables de instancia
        double op1, op2, opr, res;
        String total;
        char c;
        //System.out.println(exp.charAt(1));
        
        //Se inicializa el ciclo
        for (int i = 0; i < exp.length(); i++) {
            //"Obtenga el valor del caracter en esta posicion"
            c = exp.charAt(i);
            //Si el caracter no es un espacio, entonces haga...
            if (c != ' '){ 
                //Si es un digito entonces...
                 if(Character.isDigit(c)){ 
                    //Consiga su valor numerico
                    if(stk.size()<2){
                        opr = Character.getNumericValue(c);
                    //Mandelo al stack de enteros.
                        stk.push(opr);
                    } else{
                        return "Error de sintaxis, ingrese 2 numeros unicamente.";
                    }
                    
                }
                //Si es "+" y el stack tiene 2 items
                else if ((c == '+') && (stk.size() == 2)){
                    //Consiga los ultimos items del stack
                    op1 = stk.pop();
                    op2 = stk.pop();
                    //Sumelos
                    res = op1+op2;
                    //Vuelva a enviar el resultado al stack para que siga la operacion
                    stk.push(res);
                }
                //MISMO QUE LA SUMA
                //Solo cambia el operador y operacion. Pero la base es la misma.
                else if ((c == '-') && (stk.size() == 2)){ 
                    op2 = stk.pop();
                    op1 = stk.pop();
                    res = op1-op2;
                    stk.push(res);
                }
                else if ((c == '*') && (stk.size() == 2)){ 
                    op1 = stk.pop();
                    op2 = stk.pop();
                    res = op1*op2;
                    stk.push(res);
                }
                else if ((c == '/') && (stk.size() == 2)){ 
                    op2 = stk.pop();
                    op1 = stk.pop();
                    res = op1/op2;
                    stk.push(res);
                }
            }
        }
        
        //Despliegue el total
        total = stk.pop()+"";
        
        return total;
    }
    
}
