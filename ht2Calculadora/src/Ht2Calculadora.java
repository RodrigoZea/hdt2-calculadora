
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 * HT2 CALCULADORA
 * Ht2Calculadora: El metodo principal de calculadora, este pide el archivo de texto
 * @author Rodrigo Zea y Francisco Molina
 */
public class Ht2Calculadora {


    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
          //Lector de texto
  BufferedReader reader;
  //Escoge archivo
  JFileChooser menu;
  //Filtro para archivo unicamente TXT
  FileNameExtensionFilter txtOnly;
        
        
      menu = new JFileChooser();
      txtOnly= new FileNameExtensionFilter("TEXT FILES", "txt", "text");
      menu.setFileFilter(txtOnly);
      int returnV = menu.showOpenDialog(null);
        
      if (returnV == JFileChooser.APPROVE_OPTION){ 
         reader = new BufferedReader(new FileReader(menu.getSelectedFile().getAbsoluteFile()));  
         
         
        // TODO code application logic here
        Calculadora calc = new Calculadora();
        try {
            
            //String instruction=calc.importData(reader);
            
            String leer = "";
            String instruction = "";
            while ((leer = reader.readLine()) != null){ 
                instruction = instruction + leer;
            }
            
            System.out.println("El resultado es: " + calc.calcular(instruction));
            
        } catch (IOException ex) {
            System.out.println("ERROR al leer archivo.");
        }
         
      }else{ 
         
         System.out.println("Operación cancelada");
         System.exit(0);
      }
      
       
    }
    
}
